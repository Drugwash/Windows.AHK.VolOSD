# VolOSD

Adaptation of a script by **Rajat** once presented in **AutoHotkey Help Showcase**.

Controls audio volume by custom hotkeys, provides OSD feedback on volume level(s).      
Besides hotkeys there is a mini-panel activatable by single-clicking the tray icon (if enabled):            
![](ss_volosd_mini.png)

Most useful in Windows® 9x where Wave volume exists besides Master volume.

The Settings dialog can be activated by double-clicking the tray icon,      
or by using the dedicated hotkey when tray icon is hidden.

#### Settings dialog overview

![](ss_volosd_options.png) ![](ss_volosd_credits.png)
		
**1.** OSD bars' appearance can be tweaked to a certain degree.     
In XP and 2003 there is the additional choice of obeying system theme       
instead of the user-defined - or default - colors.

**2.** Hotkeys are assigned to main functions:

- Master volume up
- Master volume down
- Wave<sup>**[1]**</sup> volume up
- Wave volume down
- Master dim[ming]
- Master mute
- Exit

Additional hotkeys are available:

- Options (when **No tray icon** mode is enabled)
- Cycle soundcards (when multiple functional soundcards have been detected)

**3.** OSD can be displayed on selected monitor if more of them available.

**4.** When multiple soudcards are installed a default soundcard can be assigned,       
or the user can cycle through soundcards using the dedicated hotkey.        
Additional sound options are available:

- volume can dim to/from a preset percent
- in/out fading time is adjustable
- fading can be turned on/off

**5.** General options:

- no tray icon
- start with Windows
- remember Settings dialog position
- time for showing splash window on mute on/off

***	
	
<sup>**[1]**</sup> Wave volume is only available in older Windows® versions prior to Vista.

Enjoy!

**© Drugwash, 2008-2017, 2023**

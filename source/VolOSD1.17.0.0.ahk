; Professional VolOSD by Drugwash
; Provides a convenient GUI for
; altering Main and Wave volume
;================================================================
;#NoEnv ; can't use this anymore since adding Vista compatibility mode
#NoTrayIcon
#SingleInstance, Force
SetBatchLines, -1
SetControlDelay, -1
SetWinDelay, -1
SetFormat, Float, 0.1
ListLines, Off
CoordMode, Mouse, Screen
DetectHiddenWindows, On
SetWorkingDir := A_ScriptDir
updates()
;================================================================
; Thanks to Lexikos for the compatibility part!
; http://www.autohotkey.com/forum/topic23758.html
Loop, %0%
	clp .= A_Space A_LoopField	; CommandLine Parameters
if A_OSVersion = WIN_VISTA  ; equals WIN_XP if in XP-compatibility mode
	{
	EnvSet, __COMPAT_LAYER, WinXPSp2 RunAsAdmin
	Run, % (A_IsCompiled ? A_ScriptFullPath " /R" : A_AhkPath " /R """ A_ScriptFullPath """" clp),, UseErrorLevel
	Pause
	}
;================================================================
appname = VolOSD				; application name
version = 1.17.0.0 Pro			; version number
release = June 17, 2017			; release date
type = public					; release type can be internal or public
iconlocal = res\VolOSD4a2.ico		; external icon for uncompiled script
iconlocalM = res\VolOSD4b2.ico	; external icon for uncompiled script (Mute version)
debug := InStr(clp, "-d")			; debug switch (1=active)
debugR := !debug				; reverse debug switch
dbgP := debug ? "+" : "-"
;================================================================
myhome := "http://my" Chr(46) "cloudme" Chr(46) "com/#drugwash/mytools"
home := "http://www" Chr(46) "autohotkey" Chr(46) "com/forum/viewtopic" Chr(46) "php?t=44751"
email := "drugwash" Chr(64) "mail" Chr(46) "com"
link0 := "http://www" Chr(46) "autohotkey" Chr(46) "com"
link1 := "http://www" Chr(46) "autohotkey" Chr(46) "com/docs/scripts/VolumeOSD" Chr(46) "htm"
link2 := "http://www" Chr(46) "autohotkey" Chr(46) "com/forum/viewtopic" Chr(46) "php?p=18505#18505"
link3 := "http://www" Chr(46) "autohotkey" Chr(46) "com/forum/topic17230" Chr(46) "html"
link4 := "http://creativecommons" Chr(46) "org/licenses/by-nc/3.0/"
link5 := "http://www" Chr(46) "autohotkey" Chr(46) "com/forum/viewtopic" Chr(46) "php?t=36352"
link6 := "http://www" Chr(46) "donationcoder" Chr(46) "com/Software/Skrommel/index" Chr(46) "html"
;================================================================
if !A_IsCompiled
	{
	hIcon1 := DllCall("LoadImage" AW, Ptr, 0, "Str", iconlocal, "UInt", 1, "Int", 16, "Int", 16, "UInt", 0x2010, Ptr)
	hIcon2 := DllCall("LoadImage" AW, Ptr, 0, "Str", iconlocalM, "UInt", 1, "Int", 16, "Int", 16, "UInt", 0x2010, Ptr)
	}
else
	{
	i := DllCall("GetModuleHandle" AW, Ptr, 0)
	hIcon1 := DllCall("LoadImage" AW, Ptr, i, "UInt", 159, "UInt", 1, "Int", 16, "Int", 16, "UInt", 0x2000, Ptr)
	hIcon2 := DllCall("LoadImage" AW, Ptr, i, "UInt", 160, "UInt", 1, "Int", 16, "Int", 16, "UInt", 0x2000, Ptr)
	}
initFile := A_WorkingDir . "\VolOSD.ini"
EnvGet, i, __COMPAT_LAYER
if InStr(i, "WinXPSp")
	isVista=1
monpath := w9x ? "Enum\MONITOR" : "SYSTEM\CurrentControlSet\Enum\DISPLAY"
kroot_AR := "HKCU"
kval_AR := "Software\Microsoft\Windows\CurrentVersion\Run"
c := "MASTER|DIGITAL|LINE|MICROPHONE|SYNTH|CD|TELEPHONE|PCSPEAKER|WAVE|AUX|ANALOG|HEADPHONES|N/A"
cObjects := "Master,Wave"
;================================================================
; If your keyboard has multimedia buttons for Volume, you can
; try changing the below hotkeys to use them by specifying
; Volume_Up, ^Volume_Up, Volume_Down, and ^Volume_Down
; (Note: Win9x does not distinguish Left/Right of ALT/CTRL/SHIFT)
; The hotkey input cannot do that either, so useless to define separate 9x/XP hotkeys.
; This may change at a later time, to support key modifiers like WinKey, etc.
;================================================================
;					Default hotkeys
def_hkmUp = ^Up			; CTRL+UpArrow (master up)
def_hkmDn = ^Down		; CTRL+DownArrow (master down)
def_hkwUp = ^!Up			; ALT+UpArrow (wave up)
def_hkwDn = ^!Down		; ALT+DownArrow (wave down)
def_hkmDim = ^+Up		; CTRL+SHIFT+Up (master dim)
def_hkmMut = ^+Down		; CTRL+SHIFT+Down (master mute)
def_hkOpt = ^+o			; CTRL+SHIFT+O (options)
def_hkSC = ^+Enter		; CTRL+SHIFT+Enter
def_hkEx = ^!F12			; CTRL+ALT+F12 (exit application)
;================================================================
;					User Settings
; Make customization only in this area and/or hotkeys area !!
defNT = 0			; default tray icon state [enabled]
defAuto = 0			; default autostart with Windows [off]
defWP = 0			; default Wave volume pinning [off]
defWPR = 0			; default Wave volume pinning reset-on-exit [off]
defFM = 1			; default Master fading use [on]
defFW = 1			; default Wave fading use [on]
defSP = 1			; default Options dialog position saving [on]
defA = 1				; default soundcard
defAS = 1			; default soundcard selection
defM = 1				; default monitor
defMS = 1			; default monitor selection
mutH = -1			; default mute splash position [centered]
defMW = 200			; default mute splash width (px)
defPX = -1			; default progressbar position H [centered]
defPY = -1			; default progressbar position V [centered]
defS = 2				; default volume change step (percent)
defD = 10			; default volume when dimmed (percent)
defW = 120			; default progressbar width (px)
defH = 10			; default progressbar height (px)
defDT = 2000			; default progressbars display time (ms)
defFT = 3000			; default fading time (ms)
defCM = FF0000		; default master bar color (hex RGB)
defCW = 0000FF		; default wave bar color (hex RGB)
defCB = C0C0C0		; default bar background color (hex RGB)
defTh = 0			; default OS theme [off]
hasWave = 0			; default for WAVE check
cards = 0				; detected soundcards
;================================================================
mutW := defMW
IniWrite, %mutW%, %initFile%, Size, mutW	; temporarily
hEnumProc := RegisterCallback("DS_EnumProc", "F", 4, "")
_MB2WC("explorer", exploreW)	; prepare Unicode string for later use
OnExit, getout
gosub HWdetect
if ! FileExist(initFile)
	{
	notray := defNT		; current tray icon state
	auto := defAuto		; current autostart option
	savePos := defSP		; current Options dialog position saving
	pin_Wave := defWP	; current Wave volume pinning option
	rst_Wave := defWPR	; current Wave volume pinning reset-on-exit option
	useFadeMaster := defFM ; current Master fade use
	useFadeWave := defFW ; current Wave fade use
	aud_Def := defA		; current soundcard
	aud_DS := defAS		; current soundcard selection
	mon_Def := defM		; current display monitor number (in multi-monitor environments)
	mon_DS := defMS		; current monitor selection (in multi-monitor environments)
	mutW := defMW		; current mute splash width
	vol_PosX = defPX		; default horizontal position (center)
	vol_PosY = defPY		; default vertical position (center)
	vol_Step := defS		; current percentage by which to raise or lower the volume each time
	vol_Dim := defD		; current volume when dimmed
	vol_DT := defDT		; current progressbars display time
	vol_FT := defFT		; current volume fading time
	vol_Width := defW		; current width of bar
	vol_Thick := defH		; current thickness of bar
	vol_CBM := defCM		; current Master Volume Bar color
	vol_CBW := defCW		; current Wave Volume Bar color
	vol_CW := defCB		; current Background color
	custM := defCM		; custom Master Volume Bar color
	custB := defCW		; custom Wave Volume Bar color
	custW := defCB		; custom bar Background color
	themed = defTh		; OS theme is by default DISABLED
	hkm_Up := def_hkmUp	; current hotkey for (master up)
	hkm_Dn := def_hkmDn	; current hotkey for (master down)
	hkw_Up := def_hkwUp	; current hotkey for (wave up)
	hkw_Dn := def_hkwDn	; current hotkey for (wave down)
	hkm_Dim := def_hkmDim	; current hotkey for (master dimmed)
	hkm_Mut := def_hkmMut ; current hotkey for (master mute)
	hk_Opt := def_hkOpt	; current hotkey for (show Options)
	hk_SC := def_hkSC		; current hotkey for (cycle soundcard)
	hk_Ex := def_hkEx		; current hotkey for (exit application)
	}
else
	gosub readini
;================================================================
;					Custom tray menu
Menu, Tray, Icon, % A_IsCompiled ? "" : iconlocal
Menu, Tray, NoIcon
Menu, Tray, NoStandard
Menu, Tray, Add, Settings, options
Menu, Tray, Default, Settings
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, getout
title := "11|" appname " v" version
if !notray
	{
	gosub setTray
	_TrayIconAct("options", 0, 0, 0)
	}
title := ""
gosub applyHK
gosub createGUI
gosub applyVal
gosub getmute	; only to pass TrayVolWnd handle for the tray tip
GuiControl, 3:, vol_Master, %vol_Master%
GuiControl, 3:, vol_Wave, %vol_Wave%
OnMessage(DllCall("RegisterWindowMessage", "Str", "TaskbarCreated", "UInt"), "retray")	; add back tray icon on shell restart
;================================================================
;			DON'T CHANGE ANYTHING HERE (unless you know what you're doing).
autoex:				; Auto Execute Section
;================================================================
vol_BarOptionsMaster = B ZH%vol_Thick% ZX0 ZY0 W%vol_Width% CW%vol_CW% CB%vol_CBM%
vol_BarOptionsWave   = B ZH%vol_Thick% ZX0 ZY0 W%vol_Width% CW%vol_CW% CB%vol_CBW%

; If the X position has been specified, add it to the options.
; Otherwise, omit it to center the bar horizontally:
if vol_PosX
	{
	vol_BarOptionsMaster = %vol_BarOptionsMaster% X%vol_PosX%
	vol_BarOptionsWave   = %vol_BarOptionsWave% X%vol_PosX%
	}

; If the Y position has been specified, add it to the options.
; Otherwise, omit it to have it calculated later:
if vol_PosY
	{
	vol_BarOptionsMaster = %vol_BarOptionsMaster% Y%vol_PosY%
	vol_PosY_wave = %vol_PosY%
	vol_PosY_wave += %vol_Thick%
	vol_BarOptionsWave = %vol_BarOptionsWave% Y%vol_PosY_wave%
	}
return
;================================================================
vol_WaveUp:			; Sound changes apply here
;================================================================
Clip =
SoundSet, +%vol_Step%, Wave, Volume, %aud_DS%
goto vol_ShowBars
;================================================================
vol_WaveDown:
Clip =
SoundSet, -%vol_Step%, Wave, Volume, %aud_DS%
goto vol_ShowBars
;================================================================
vol_MasterUp:
Clip =
SoundSet, +%vol_Step%, Master, Volume, %aud_DS%
goto vol_ShowBars
;================================================================
vol_MasterDown:
Clip =
SoundSet, -%vol_Step%, Master, Volume, %aud_DS%
goto vol_ShowBars
;================================================================
vol_Dimmer:
if (Clip := !Clip)
	SoundGet, tmpV, Master, Volume, %aud_DS%
SoundSet, % (Clip ? vol_Dim : tmpV), Master, Volume, %aud_DS%
goto vol_ShowBars
;================================================================
vol_WaveMute:
obj := "Wave"
goto vol_MuteCommon
;================================================================
vol_MasterMute:
obj := "Master"
vol_MuteCommon:
Clip =
WinGet, shell, ID, ahk_class Shell_TrayWnd	; not sure it's needed but if shell is restarted...
if up%obj% = 1
	SoundSet, 0, %obj%, MUTE, %aud_DS%
if useFade%obj%
	gosub vol_Fading
if up%obj% = -1
	{
	if !isVista
		SoundSet, -1, %obj%, MUTE, %aud_DS%
	; WM_APPCOMMAND, APPCOMMAND_VOLUME_MUTE
	else DllCall("SendMessageW", Ptr, shell, "UInt", 0x319, "UInt", 0, "UInt", 0x80000)
	}
mut%obj% := mut%obj%="2" ? "1" : "2"
i := "hIcon" mut%obj%
DllCall("SendMessage" AW, Ptr, hmut%obj%, "UInt", 0xF7, "UInt", 1, Ptr, i)	; BM_SETIMAGE
goto getmute
;================================================================
vol_Fading:
gosub getval
StringLeft, i, obj, 1
if ((vol_%obj% < 1 && up%obj% = -1) OR (vol_%obj% > 1 && up%obj% = 1))
	return
if !vol%i%
	vol%i% := vol_%obj% < 1 ? "50" : v%i%
gosub vol_BarOff
HotKey, %hkm_Up%, off
HotKey, %hkm_Dn%, off
Hotkey, %hkm_Dim%, off
Hotkey, %hkm_Mut%, off
step := vol_FT / vol%i% - 3
IfWinNotExist, vol_%obj%
	Progress,  % "3:" vol_BarOptions%obj%, , , vol_%obj%
Loop, % vol%i%
	{
	t1 := A_TickCount
	vol_%obj% += up%obj%
	SoundSet, % vol_%obj%, %obj%, Volume, %aud_DS%
	Progress, % "3:" vol_%obj%
	GuiControl, 3:, vol_%obj%, % vol_%obj%
	Loop
		if (step+t1 < A_TickCount)
			break
	}
gosub vol_BarOff
HotKey, %hkm_Up%, on
HotKey, %hkm_Dn%, on
Hotkey, %hkm_Dim%, on
Hotkey, %hkm_Mut%, on
return
;================================================================
3GuiEscape:			; Close & Exit
Gui, 3:Hide
return
;================================================================
showTray:
;================================================================
MouseGetPos, x, y
WinGetPos,,, w, h, ahk_id %TrayVolWnd%
x1 := Mon%mon_Def%Right - w
y1 := Mon%mon_Def%Bottom - h
x := x > x1 ? x1 : x
y := y > y1 ? y1 : y
Gui, 3:Show, x%x% y%y%
return
;================================================================
GuiEscape:
ButtonCancel:
;================================================================
gosub readini
gosub autoex
vol_DT := custDT
goto GuiClose
;================================================================
ButtonOK:
;================================================================
Gui, Submit, NoHide
if change
	gosub autostart
vol_DT := custDT
if change
	gosub saveini

GuiClose:
GuiControl, Disabled, &Cancel
gosub applyHK
HotKey, %hkm_Up%, on
HotKey, %hkm_Dn%, on
if hasWave
	{
	HotKey, %hkw_Up%, on
	HotKey, %hkw_Dn%, on
	}
Hotkey, %hkm_Dim%, on
Hotkey, %hkm_Mut%, on
Hotkey, %hk_Opt%, on
Hotkey, %hk_SC%, on
Hotkey, %hk_Ex%, on
gosub vol_BarOff
WinGetPos, ODx, ODy,,, VolOSD options
if savePos
	{
	IniWrite, %ODx%, %initFile%, General, ODx
	IniWrite, %ODy%, %initFile%, General, ODy
	}
Gui, Hide
change =
return
;================================================================
reload:
;gosub cleanup
SetWorkingDir := A_ScriptDir
Run, %A_ScriptFullPath%, A_WorkingDir, UseErrorLevel
ExitApp
;================================================================
getout:
gosub cleanup
ExitApp
;================================================================
cleanup:
;================================================================
if !appname
	return	; trying to avoid weird reload crash under Win9x
SetTimer, chkWave, off
TrayIconSet(-1, TrayVolWnd)
OnMessage(0x200,"")
;DllCall("DestroyCursor", Ptr, hCurs)	; DON'T DESTROY SHARED CURSORS!!!
;DllCall("DestroyCursor", Ptr, hCursI)	; System does it on process exit.
DllCall("DestroyIcon", Ptr, hIcon1)
DllCall("DestroyIcon", Ptr, hIcon2)
Loop, %cards%
	if (hasWave%A_Index% && rst_Wave)
		SoundSetWaveVolume, hasWave%A_Index%, %A_Index%
return
;================================================================
vol_ShowBars:			; Display
;================================================================
if mon_DS = 2
	gosub getpos	; find the active window's screen and position OSD according to user settings
gosub autoex

if (oldPosX - vol_PosX)
	gosub vol_BarOff
oldPosX := vol_PosX

; To prevent the "flashing" effect, only create the bar window if it doesn't already exist
if hasWave
	{
	If !WinExist("vol_Wave")
		Progress, 2:%vol_BarOptionsWave%, , , vol_Wave
	WinGet, i, ID, vol_Wave ahk_class AutoHotkey2
	DllCall("SendMessage" AW, Ptr, i, "UInt", 0x410, "UInt", 1, "UInt", 0)	; PBM_SETSTATE PBST_NORMAL
	}
If !WinExist("vol_Master")
	{
	; Calculate position here in case screen resolution changes
	; while the script is running:
	if vol_PosY < 0
		{
		; Create the Master bar just above the Wave bar:
		WinGetPos, , vol_Wave_Posy, , , vol_Wave
		vol_Wave_Posy -= %vol_Thick%
		Progress, 3:%vol_BarOptionsMaster% Y%vol_Wave_Posy%, , , vol_Master
		}
	else
		Progress, 3:%vol_BarOptionsMaster%, , , vol_Master
	}
WinGet, i, ID, vol_Master ahk_class AutoHotkey2
DllCall("SendMessage" AW, Ptr, i, "UInt", 0x410, "UInt", 2, "UInt", 0)	; PBM_SETSTATE PBST_ERROR
; unmute when any of the volume controls is changed
Loop, Parse, cObjects, CSV
	if A_LoopField in Master,%obj%
		if mut%A_LoopField%=2
			{
			SoundSet, 0, %A_LoopField%, Mute, %aud_DS%
			mut%A_LoopField% = 1
			muted%A_LoopField% := "OFF"
			DllCall("SendMessage" AW, Ptr, hmut%A_LoopField%, "UInt", 0xF7, "UInt", 1, Ptr, hIcon1)	; BM_SETIMAGE
			}
; Get both volume levels in case the user or an external program changed them:
gosub getval
Progress, 1:Off
if !notray
	gosub setTray
Progress, 3:%vol_Master%
if hasWave
	Progress, 2:%vol_Wave%
GuiControl, 3:, vol_Master, %vol_Master%
GuiControl, 3:, vol_Wave, %vol_Wave%
SetTimer, vol_BarOff, %vol_DT%
return
;================================================================
vol_BarOff:
SetTimer, vol_BarOff, off
Progress, 1:Off
if hasWave
	Progress, 2:Off
Progress, 3:Off
return
;================================================================
HWdetect:			; Get multi-monitor info
;================================================================
detmon()
gosub getmon
SysGet, FW, 78	; full width SM_CXVIRTUALSCREEN
SysGet, FH, 79	; full height SM_CYVIRTUALSCREEN
if debug
	MsgBox, 0x42040, %appname%,
		(LTrim
		Total monitors: %MonTot% (variable removed)
		Total active monitors: %MonAct%
		Primary monitor: %MonPrim%
		Name1: %MonNam1%
		Name2: %MonNam2%
		Name3: %MonNam3%
		Name4: %MonNam4%
		
		%Mon1Left%,%Mon1Top%
		%Mon2Left%,%Mon2Top%
		%Mon3Left%,%Mon3Top%
		%Mon4Left%,%Mon4Top%
		)
;================================================================
;					Get soundcard info
DllCall("dsound\DirectSoundEnumerate" AW, Ptr, hEnumProc, "UInt", 0)
if ! cards
	{
	MsgBox, 0x42030, %appname%,
		(LTrim
		No soundcards could be found.
		There is no point in running under this configuration.
		The application will now exit.
		)
	ExitApp
	}
;================================================================
getmute:				; Get mute state
SoundGet, initM, Master, MUTE, %aud_DS%	; need a workaround for this in Win7
StringUpper, initM, initM
mutMaster := initM="ON" ? "2" : "1"
if hasWave%aud_DS%
	{
	SoundGet, initW, Wave, MUTE, %aud_DS%
	StringUpper, initW, initW
	}
else initW=
mutWave := initW="ON" ? "2" : "1"
gosub getpos
gosub vol_BarOff
Mtxt := "Master mute: " initM (hasWave ? "`nWave Mute: " initW : "")
Progress, % "1:B2 ZH0 FM14 CTFF00FF CW0 " (mutH < 1 ? "" : "X" mutH) " W" mutW,,%Mtxt%,,Arial
SetTimer, vol_BarOff, %vol_DT%
getval:
SoundGet, vol_Master, Master, VOLUME, %aud_DS%
vM := Round(vol_Master)
upMaster := mutMaster="2" ? "1" : "-1"
if hasWave%aud_DS%
	SoundGet, vol_Wave, Wave, VOLUME, %aud_DS%
vW := Round(vol_Wave)
upWave := mutWave="2" ? "1" : "-1"
if !notray
	gosub setTray
return
;================================================================
getpos:				; Get active window coords
;================================================================
WinGetPos, ActX, ActY, ActW, ActH, A	; get coords of active window
gosub getmon
Loop, %MonAct%
	if compare(ActX, ActY, ActW, ActH, Mon%A_Index%Left, Mon%A_Index%Top, Mon%A_Index%Right, Mon%A_Index%Bottom)
		break
return
;================================================================
monsel:				; Get display position
;================================================================
Gui Submit, NoHide
if debug
	MsgBox, mm1=%mm1%`nmm2=%mm2%`nmm3=%mm3%`nmm4=%mm4%`n`nMonAct=%MonAct%
Loop, %MonAct%
	if mm%A_Index%
		fm := A_Index
	vol_PosX := Mon%fm%Left + (Mon%fm%Right - Mon%fm%Left) // 2 - (vol_Width // 2)
	vol_PosY := Mon%fm%Top + (Mon%fm%Bottom - Mon%fm%Top) // 2 - vol_Thick
	mutH := Mon%fm%Left + (Mon%fm%Right - Mon%fm%Left) // 2 - (mutW // 2)
return
;================================================================
getmon:				; Get monitor info
;================================================================
Loop, %MonAct%
	SysGet, Mon%A_Index%, Monitor, %A_Index%
return
;================================================================
applyVal:				; Apply settings (either default or from ini)
;================================================================
GuiControl,, m%mon_DS%, 1
gosub ms%mon_DS%
GuiControl,, s%aud_DS%, 1
gosub ss%aud_DS%
change =			; trigger for any change in the options
GuiControl, Disabled, &Cancel
return
;================================================================
applyHK:				; Apply hotkeys
;================================================================
HotKey, %hkm_Up%, vol_MasterUp	; current hotkey for (master up)
HotKey, %hkm_Dn%, vol_MasterDown	; current hotkey for (master down)
if hasWave
	{
	HotKey, %hkw_Up%, vol_WaveUp	; current hotkey for (wave up)
	HotKey, %hkw_Dn%, vol_WaveDown	; current hotkey for (wave down)
	}
Hotkey, %hkm_Dim%, vol_Dimmer		; current hotkey for (master dimmed)
Hotkey, %hkm_Mut%, vol_MasterMute	; current hotkey for (master mute)
Hotkey, %hk_Opt%, options			; current hotkey for (show Options)
Hotkey, %hk_SC%, cycleCard		; current hotkey for (cycle soundcard)
Hotkey, %hk_Ex%, getout			; current hotkey for (exit application)
return
;================================================================
options:				; Settings
;================================================================
HotKey, %hkm_Up%, off
HotKey, %hkm_Dn%, off
if hasWave
	{
	HotKey, %hkw_Up%, off
	HotKey, %hkw_Dn%, off
	}
Hotkey, %hkm_Dim%, off
Hotkey, %hkm_Mut%, off
Hotkey, %hk_Opt%, off
Hotkey, %hk_SC%, off
Hotkey, %hk_Ex%, off
custDT := vol_DT
vol_DT = 60000
Gui, Show, % (savePos ? "x" (ODx < FW-300 ? ODx : "0") " y" (ODy < FH-300 ? ODy : "Center") " " : "x0 yCenter ") "h338 w335", VolOSD options
if ! hCurs
	{
	hCurs := DllCall("LoadCursor", Ptr, NULL, "Int", 32649, Ptr)	; IDC_HAND
	OnMessage(0x200, "WM_MOUSEMOVE")
	}
gosub vol_ShowBars
return
;================================================================
setW:				; OSD width setup
;================================================================
if (vol_Width - dimW = 0)
	return
vol_Width := dimW		; set bar width
GuiControl,, labelW, %vol_Width% px.
gosub refresh
goto ch

resetW:
if (vol_Width - defW = 0)
	return
vol_Width := defW		; reset bar width to default
GuiControl,, dimW, %vol_Width%
GuiControl,, labelW, %vol_Width% px.
gosub refresh
goto ch
;================================================================
setH:				; OSD height setup
;================================================================
if (vol_Thick - dimH = 0)
	return
vol_Thick := dimH		; set bar thickness
GuiControl,, labelH, %vol_Thick% px.
gosub refresh
goto ch

resetH:
if (vol_Thick - defH = 0)
	return
vol_Thick := defH		; reset bar thickness to default
GuiControl,, dimH, %vol_Thick%
GuiControl,, labelH, %vol_Thick% px.
gosub refresh
goto ch
;================================================================
pickit:				; OSD colors setup
;================================================================
if (A_GuiEvent != "Normal")
	return
colorU := SubStr(ChgColor("0x" vol_%A_GuiControl%), 3)
if ErrorLevel
	return
else vol_%A_GuiControl% := colorU
GuiControl, +Background%colorU%, %A_GuiControl%
GuiControl, 3:+c%vol_CBM% +Background%vol_CW% +Smooth, vol_Master
GuiControl, 3:+c%vol_CBW% +Background%vol_CW% +Smooth, vol_Wave
gosub refresh
goto ch

resetC:
if (vol_CBM = defCM && vol_CBW = defCW && vol_CW = defCB)
	return
vol_CBM := defCM		; reset Master Volume Bar color to default
vol_CBW := defCW		; reset Wave Volume Bar color to default
vol_CW := defCB		; reset Background color to default
GuiControl, +Background%vol_CBM%, CBM
GuiControl, +Background%vol_CBW%, CBW
GuiControl, +Background%vol_CW%, CW
GuiControl, 3:+c%vol_CBM% +Background%vol_CW% +Smooth, vol_Master
GuiControl, 3:+c%vol_CBW% +Background%vol_CW% +Smooth, vol_Wave
gosub refresh
goto ch
;================================================================
setD:				; Dimmer time setup
;================================================================
if (vol_Dim - dimD = 0)
	return
vol_Dim := dimD		; set dimmer value
GuiControl,, labelD, %vol_Dim% `%
gosub refresh
goto ch

resetD:
if (vol_Dim - defD = 0)
	return
vol_Dim := defD		; reset dimmer to default
GuiControl,, dimD, %vol_Dim%
GuiControl,, labelD, %vol_Dim% `%
gosub refresh
goto ch
;================================================================
setF:				; Fading time setup
;================================================================
if (vol_FT - fadeT = 0)
	return
vol_FT := fadeT		; set fader value
goto comF

resetF:
if (vol_FT - defFT = 0)
	return
vol_FT := defFT		; reset fader to default
GuiControl,, fadeT, %vol_FT%
comF:
GuiControl,, labelF, % vol_FT / 1000 " s"
gosub refresh
goto ch
;================================================================
ms1:					; Monitor selection setup
;================================================================
GuiControl, Show, mm1
GuiControl,, mm1, 1
GuiControl, Hide, mm2
GuiControl, Hide, mm3
GuiControl, Hide, mm4
mon_DS = 1
gosub monsel
goto ch

ms2:
Loop, 4
	GuiControl, Hide, mm%A_Index%
mon_DS = 2
goto ch

ms3:
Loop, %MonAct%
	GuiControl, Show, mm%A_Index%
mon_DS = 3
goto ch
;================================================================
ss1:					; Soundcard selection setup
ss2:
ss3:
ss4:
ss5:
ss6:
;================================================================
aud_DS := SubStr(A_ThisLabel, 3)
;goto ch

ch:
change = 1
GuiControl, Enabled, &Cancel
return
;================================================================
cycleCard:
card := (card > cards) ? 1 : ++card
if debug
	MsgBox, current soundcard=%card%
return
;================================================================
refresh:
gosub vol_BarOff
gosub vol_ShowBars
return
;================================================================
updST:
Gui, Submit, NoHide
show := showtime * 1000
if (custDT - show = 0)
	return
custDT := show
goto ch
;================================================================
theme:
Gui, Submit, NoHide
if themed
	{
	custM := vol_CBM
	custW := vol_CBW
	custB := vol_CW
	vol_CBM = Default
	vol_CBW = Default
	vol_CW = Default
	GuiControl, Hide, CBM
	GuiControl, Hide, CBW
	GuiControl, Hide, CW
	GuiControl, Hide, Master:
	GuiControl, Hide, Wave:
	GuiControl, Hide, Background:
	GuiControl, Hide, resetC
	if !w9x
		{
		DllCall("uxtheme\SetWindowTheme", Ptr, TrayM, Ptr, &exploreW, "UInt", 0)
		DllCall("uxtheme\SetWindowTheme", Ptr, TrayW, Ptr, &exploreW, "UInt", 0)
		}
	}
else
	{
	vol_CBM := custM
	vol_CBW := custW
	vol_CW := custB
	GuiControl, Show, CBM
	GuiControl, Show, CBW
	GuiControl, Show, CW
	GuiControl, Show, Master:
	GuiControl, Show, Wave:
	GuiControl, Show, Background:
	GuiControl, Show, resetC
	GuiControl, 3:+c%vol_CBM% +Background%vol_CW% +Smooth, vol_Master
	GuiControl, 3:+c%vol_CBW% +Background%vol_CW% +Smooth, vol_Wave
	}
GuiControl, +Background%vol_CBM%, CBM
GuiControl, +Background%vol_CBW%, CBW
GuiControl, +Background%vol_CW%, CW
gosub refresh
goto ch
;================================================================
notray:
if notray
	gosub setTray
else
	TrayIconSet(-1, TrayVolWnd)
GuiControl, % (notray ? "Disable" : "Enable"), hk_Opt
GuiControl, % (notray ? "Disable" : "Enable"), Options:
notray := !notray
gosub getmute
goto ch
;================================================================
setTray:
timeout=6
tip := "Master: " vM "`%" (hasWave%aud_DS% ? " � Wave: " vW "`%" : "")
	. " � Mute is " initM  (hasWave%aud_DS% ? "/" initW : "")
;title := "11|" appname " v" version
TrayIconSet(1, TrayVolWnd, "159|" iconlocal, 0x6645, "LSLshowTray|LDLoptions|RSMTray")
TrayIconTip(1, TrayVolWnd, tip, title, timeout)
DllCall("SendMessage" AW, Ptr, hmutMaster, "UInt", 0xF7, "UInt", 1, Ptr, hIcon%mutMaster%)	; BM_SETIMAGE
DllCall("SendMessage" AW, Ptr, hmutWave, "UInt", 0xF7, "UInt", 1, Ptr, hIcon%mutWave%)	; BM_SETIMAGE
return
;================================================================
autostart:
IfInString, A_ScriptFullPath, A_Space
	autopath := """ . A_ScriptFullPath . """
else
	autopath := A_ScriptFullPath
RegRead, isauto, %kroot_AR%, %kval_AR%, VolOSD
if (! isauto && auto)
	RegWrite, REG_SZ, %kroot_AR%, %kval_AR%, VolOSD, %autopath%
else if (isauto && ! auto)
	{
	MsgBox, 0x42034, Attention !!!,
		(LTrim
		Are you sure you want to delete this registry value?

		%kroot_AR%\%kval_AR%\VolOSD > %isauto%
		)
	IfMsgBox Yes
		RegDelete, %kroot_AR%, %kval_AR%, VolOSD
	}
return
;================================================================
pinWave:
pin_Wave := !pin_Wave
SetTimer, chkWave, % (pin_Wave && hasWave) ? 1000 : off
goto ch
;================================================================
chkWave:
SoundGet, i, WAVE, VOLUME, %aud_DS%
if (i != vol_Wave)
	SoundSetWaveVolume, %vol_Wave%, %aud_DS%
return
;================================================================
rstWave:
rst_Wave := !rst_Wave
goto ch
;================================================================
setFade:
%A_GuiControl% := !%A_GuiControl%
GuiControl, % (useFadeMaster | useFadeWave ? "Enable" : "Disable"), lblF
GuiControl, % (useFadeMaster | useFadeWave ? "Enable" : "Disable"), labelF
GuiControl, % (useFadeMaster | useFadeWave ? "Enable" : "Disable"), fadeT
GuiControl, % (useFadeMaster | useFadeWave ? "Enable" : "Disable"), resetF
goto ch
;================================================================
readini:				;  INI read operations
;================================================================
IniRead, notray, %initFile%, General, notray, 0			; current tray icon state (default: enabled)
IniRead, auto, %initFile%, General, auto, 0				; current autostart with Windows (default: off)
IniRead, savePos, %initFile%, General, savePos, 1		; current Options dialog position saving (default: on)
IniRead, ODx, %initFile%, General, ODx, 50				; current Options dialog H position (default: x50)
IniRead, ODy, %initFile%, General, ODy, 50				; current Options dialog V position (default: y50)
IniRead, pin_Wave, %initFile%, General, pinWave, 0		; current Wave volume pinning (default: off)
IniRead, rst_Wave, %initFile%, General, rstWave, 0		; current Wave volume pinning reset-on-exit (default: off)
IniRead, useFadeMaster, %initFile%, General, useFM, 1	; current Master fading option (default: on)
IniRead, useFadeWave, %initFile%, General, useFW, 1	; current Wave fading option (default: on)
IniRead, aud_Def, %initFile%, Hardware, aud_Def, 1		; current soundcard (default: 1)
IniRead, aud_DS, %initFile%, Hardware, aud_DS, 1		; current soundcard selection (default: 1)
IniRead, mon_Def, %initFile%, Hardware, mon_Def, 1		; current monitor (default: 1)
IniRead, mon_DS, %initFile%, Hardware, mon_DS, 1		; current monitor selection (default: 1)
IniRead, vol_PosX, %initFile%, Position, vol_PosX, -1		; current progressbar position H (default: centered)
IniRead, vol_PosY, %initFile%, Position, vol_PosY, -1		; current progressbar position V (default: centered)
IniRead, vol_Step, %initFile%, Ranges, vol_Step, 2		; current volume change step (default: 2%)
IniRead, vol_Dim, %initFile%, Ranges, vol_Dim, 10		; current volume when dimmed (default: 10%)
IniRead, vol_DT, %initFile%, Range, vol_DTs, 2000		; current progressbars display time (default 2000 ms)
IniRead, vol_FT, %initFile%, Ranges, vol_FT, 3000		; current volume fading time (default 3000 ms)
IniRead, mutW, %initFile%, Size, mutW, 200			; current mute splash width (default: 140 px)
IniRead, vol_Width, %initFile%, Size, vol_Width, 120		; current width of bar (default: 120 px)
IniRead, vol_Thick, %initFile%, Size, vol_Thick, 10		; current thickness of bar (default: 10 px)
IniRead, vol_CBM, %initFile%, Colors, vol_CBM, FF0000	; current Master Volume Bar color (default: 0xFF0000)
IniRead, vol_CBW, %initFile%, Colors, vol_CBW, 0000FF	; current Wave Volume Bar color (default: 0x0000FF)
IniRead, vol_CW, %initFile%, Colors, vol_CW, C0C0C0	; current Background color (default: 0xC0C0C0)
IniRead, custM, %initFile%, Colors, custM, FF0000		; custom Master Volume Bar color (default: 0xFF0000)
IniRead, custB, %initFile%, Colors, custB, 0000FF		; custom Wave Volume Bar color (default: 0x0000FF)
IniRead, custW, %initFile%, Colors, custW, C0C0C0		; custom Background color (default: 0xC0C0C0)
IniRead, themed, %initFile%, Colors, themed, 0			; current OS theme obedience (default: disabled)
IniRead, hkm_Up, %initFile%, Hotkeys, hkm_Up, ^Up		; current hotkey for (master up) CTRL+UpArrow
IniRead, hkm_Dn, %initFile%, Hotkeys, hkm_Dn, ^Down	; current hotkey for (master down) CTRL+DownArrow
IniRead, hkw_Up, %initFile%, Hotkeys, hkw_Up, ^!Up	; current hotkey for (wave up) ALT+UpArrow
IniRead, hkw_Dn, %initFile%, Hotkeys, hkw_Dn, ^!Down	; current hotkey for (wave down) ALT+DownArrow
IniRead, hkm_Dim, %initFile%, Hotkeys, hkm_Dim, ^+Up	; current hotkey for (master dimmed) CTRL+SHIFT+Up
IniRead, hkm_Mut, %initFile%, Hotkeys, hkm_Mut, ^+Down ; current hotkey for (master mute) CTRL+SHIFT+Down
IniRead, hk_Opt, %initFile%, Hotkeys, hk_Opt, ^+o		; current hotkey for (show Options) CTRL+SHIFT+O
IniRead, hk_SC, %initFile%, Hotkeys, hk_SC, ^+Enter	; current hotkey for (cycle soundcard) CTRL+SHIFT+Enter
IniRead, hk_Ex, %initFile%, Hotkeys, hk_Ex, ^!F12		; current hotkey for (exit application) CTRL+ALT+F12
return
;================================================================
saveini:				; INI save operations
;================================================================
IniWrite, %notray%, %initFile%, General, notray
IniWrite, %auto%, %initFile%, General, auto
IniWrite, %savePos%, %initFile%, General, savePos
IniWrite, %pin_Wave%, %initFile%, General, pinWave
IniWrite, %rst_Wave%, %initFile%, General, rstWave
IniWrite, %useFadeMaster%, %initFile%, General, useFM
IniWrite, %useFadeWave%, %initFile%, General, useFW
IniWrite, %aud_Def%, %initFile%, Hardware, aud_Def
IniWrite, %aud_DS%, %initFile%, Hardware, aud_DS
IniWrite, %mon_Def%, %initFile%, Hardware, mon_Def
IniWrite, %mon_DS%, %initFile%, Hardware, mon_DS
IniWrite, %vol_PosX%, %initFile%, Position, vol_PosX
IniWrite, %vol_PosY%, %initFile%, Position, vol_PosY
IniWrite, %vol_Step%, %initFile%, Ranges, vol_Step
IniWrite, %vol_Dim%, %initFile%, Ranges, vol_Dim
IniWrite, %vol_DT%, %initFile%, Ranges, vol_DT
IniWrite, %vol_FT%, %initFile%, Ranges, vol_FT
IniWrite, %mutW%, %initFile%, Size, mutW
IniWrite, %vol_Width%, %initFile%, Size, vol_Width
IniWrite, %vol_Thick%, %initFile%, Size, vol_Thick
IniWrite, %vol_CBM%, %initFile%, Colors, vol_CBM
IniWrite, %vol_CBW%, %initFile%, Colors, vol_CBW
IniWrite, %vol_CW%, %initFile%, Colors, vol_CW
IniWrite, %custM%, %initFile%, Colors, custM
IniWrite, %custB%, %initFile%, Colors, custB
IniWrite, %custW%, %initFile%, Colors, custW
IniWrite, %themed%, %initFile%, Colors, themed
IniWrite, %hkm_Up%, %initFile%, Hotkeys, hkm_Up
IniWrite, %hkm_Dn%, %initFile%, Hotkeys, hkm_Dn
IniWrite, %hkw_Up%, %initFile%, Hotkeys, hkw_Up
IniWrite, %hkw_Dn%, %initFile%, Hotkeys, hkw_Dn
IniWrite, %hkm_Dim%, %initFile%, Hotkeys, hkm_Dim
IniWrite, %hkm_Mut%, %initFile%, Hotkeys, hkm_Mut
IniWrite, %hk_Opt%, %initFile%, Hotkeys, hk_Opt
IniWrite, %hk_SC%, %initFile%, Hotkeys, hk_SC
IniWrite, %hk_Ex%, %initFile%, Hotkeys, hk_Ex
return
;================================================================
createGUI:			; GUI creation
;================================================================
Gui, Font, s8, Tahoma
Gui, Add, Tab2, x5 y5 w326 h300 -Wrap -Theme -Background -0x800 0x3040 AltSubmit
	, Appearance|Hotkeys|Monitor|Soundcard|General|About
Gui, Tab
Gui, Add, Button, x194 y310 w62 h22 Default, &OK
Gui, Add, Button, x258 y310 w62 h22 Disabled, &Cancel
;================================================================
;					TAB 1 (Appearance)
Gui, Tab, 1
Gui, Add, GroupBox, x11 y+5 w312 h135, Size
Gui, Add, Text, x20 y50 h16, Progress bar width:
Gui, Add, Text, x+3 yp h16 vlabelW, %vol_Width% px.
Gui, Add, Button, x245 yp-2 w62 h22 gresetW, Default
Gui, Add, Slider, x20 y75 w290 h25 vdimW gsetW AltSubmit Range50-500 Thick18 TickInterval18 Line10 Page20 ToolTip, %vol_Width%
Gui, Add, Text, x20 y105 h16, Progress bar height:
Gui, Add, Text, x+3 yp h16 vlabelH, %vol_Thick% px.
Gui, Add, Button, x245 yp-2 w62 h22 gresetH, Default
Gui, Add, Slider, x20 y130 w290 h25 vdimH gsetH AltSubmit Range5-30 Thick18 TickInterval1 Line1 Page2 ToolTip, %vol_Thick%
Gui, Add, GroupBox, x11 y170 w310 h122, Colors
i=
if !debug
	if A_OSVersion in WIN_NT4,WIN_95,WIN_98,WIN_ME,WIN_2000
		{
		i := "Disabled"
		themed=0
		}
Gui, Add, Checkbox, x20 y193 w100 h19 Checked%themed% %i% vthemed gtheme AltSubmit, Obey OS theme
Gui, Add, Button, x245 y190 w62 h22 Hidden%themed% vresetC gresetC, Default
Gui, Add, Text, x180 y218 w60 h14 Right Hidden%themed%, Master:
Gui, Add, ListView, x+5 yp-3 w60 h20 -E0x200 +Border -hdr +ReadOnly Hidden%themed% Background%vol_CBM% AltSubmit vCBM gpickit
Gui, Add, Text, x180 y243 w60 h14 Right Hidden%themed%, Wave:
Gui, Add, ListView, x+5 yp-3 w60 h20 -E0x200 +Border -hdr +ReadOnly Hidden%themed% Background%vol_CBW% AltSubmit vCBW gpickit
Gui, Add, Text, x180 y268 w60 h14 Right Hidden%themed%, Background:
Gui, Add, ListView, x+5 yp-3 w60 h20 -E0x200 +Border -hdr +ReadOnly Hidden%themed% Background%vol_CW% AltSubmit vCW gpickit
;================================================================
;					TAB 2 (Hotkeys)
Gui, Tab, 2
hkw = 130
hkh = 18
Gui, Font, s8
Gui, Add, GroupBox, x11 y+5 w312 h260, Hotkeys
Gui, Font, s7
Gui, Add, Text, x20 y48 w90 h14 Right, Master up:
Gui, Add, Hotkey, x+5 yp-2 w%hkw% h%hkh% vhkm_Up gch, %hkm_Up%
Gui, Add, Text, x20 y+4 w90 h14 Right, Master down:
Gui, Add, Hotkey, x+5 yp-2 w%hkw% h%hkh% vhkm_Dn gch, %hkm_Dn%
Gui, Add, Text, % "x20 y+4 w90 h14 Right" (hasWave ? "" : " Disabled"), Wave up:
Gui, Add, Hotkey, % "x+5 yp-2 w" hkw " h" hkh (hasWave ? "" : " Disabled") " vhkw_Up gch", %hkw_Up%
Gui, Add, Text, % "x20 y+4 w90 h14 Right" (hasWave ? "" : " Disabled"), Wave down:
Gui, Add, Hotkey, % "x+5 yp-2 w" hkw " h" hkh (hasWave ? "" : " Disabled") " vhkw_Dn gch", %hkw_Dn%
Gui, Add, Text, x20 y+4 w90 h14 Right, Master dim:
Gui, Add, Hotkey, x+5 yp-2 w%hkw% h%hkh% vhkm_Dim gch, %hkm_Dim%
Gui, Add, Text, x20 y+4 w90 h14 Right, Mute:
Gui, Add, Hotkey, x+5 yp-2 w%hkw% h%hkh% vhkm_Mut gch, %hkm_Mut%
Gui, Add, Text, x20 y+4 w90 h14 Right Disabled, Options:
Gui, Add, Hotkey, x+5 yp-2 w%hkw% h%hkh% Disabled vhk_Opt gch, %hk_Opt%
Gui, Add, Text, x20 y+4 w90 h14 Right Disabled, Cycle soundcards:
Gui, Add, Hotkey, x+5 yp-2 w%hkw% h%hkh% Disabled vhk_SC gch, %hk_SC%
Gui, Add, Text, x20 y+4 w90 h14 Right, Exit:
Gui, Add, Hotkey, x+5 yp-2 w%hkw% h%hkh% vhk_Ex gch, %hk_Ex%
Gui, Font, s8
Gui, Add, Text, x20 y245 w290 h45 Center,
	(LTrim Join
	Note: The 'Options' hotkey is only available when there is no tray icon.
	To disable the tray icon, mark 'No tray icon' in the 'General' panel.
	)
;================================================================
;					TAB 3 (Monitor)
Gui, Tab, 3
i := 140/MonAct - 1
Gui, Add, GroupBox, x11 y+5 w312 h260, Behavior
Gui, Font, s7
Gui, Add, Radio, x20 y50 w295 h16 vm1 gms1 Checked %dbgP%Border, Always on primary monitor
Gui, Add, Radio, x20 y+1 wp hp vm2 gms2 Hidden%debugR% %dbgP%Border, Always follow active window
Gui, Add, Radio, x20 y+1 wp hp vm3 gms3 Hidden%debugR% %dbgP%Border, Use the monitor selected below:
Gui, Add, Radio, xp+15 y+2 w280 h%i% vmm1 gmonsel Group Checked Hidden%debugR% %dbgP%Border, %MonNam1%
Gui, Add, Radio, xp y+1 wp hp vmm2 gmonsel Hidden%debugR% %dbgP%Border, %MonNam2%
Gui, Add, Radio, xp y+1 wp hp vmm3 gmonsel Hidden%debugR% %dbgP%Border, %MonNam3%
Gui, Add, Radio, xp y+1 wp hp vmm4 gmonsel Hidden%debugR% %dbgP%Border, %MonNam4%
GuiControl, % (MonAct > 1 ? "Show" : "Hide"), m2
GuiControl, % (MonAct > 1 ? "Show" : "Hide"), m3
Loop, 4
	GuiControl, % (A_Index > MonAct ? "Hide" : "Show"), mm%A_Index%
Gui, Font, s8
Gui, Add, Text, x20 y257 w290 h30 Center
	, Note: Due to OS limitations`, Windows 95 && NT will always display OSD on primary monitor.
;================================================================
;					TAB 4 (Soundcard)
Gui, Tab, 4
i := 66/cards - 1
Gui, Add, GroupBox, x11 y+5 w312 h260, Device control
Gui, Font, s7
Gui, Add, Radio, x20 y50 w295 h16 %dbgP%Border Checked, Always use the following card:
Gui, Add, Radio, x20 y134 wp hp Hidden%debugR% %dbgP%Border vs0, Cycle through the following (using hotkey):
Gui, Add, Radio, xp+15 y68 w280 h%i% vs1 gss1 %dbgP%Border Group Checked, %SndNam1%
Gui, Add, Radio, xp y+1 wp hp Hidden%debugR% %dbgP%Border vs2 gss2, %SndNam2%
Gui, Add, Radio, xp y+1 wp hp Hidden%debugR% %dbgP%Border vs3 gss3, %SndNam3%
Gui, Add, Checkbox, xp y152 w280 h%i% Checked Hidden%debugR% %dbgP%Border vscard1, %SndNam1%
Gui, Add, Checkbox, xp y+1 wp hp Checked Hidden%debugR% %dbgP%Border vscard2, %SndNam2%
Gui, Add, Checkbox, xp y+1 wp hp Checked Hidden%debugR% %dbgP%Border vscard3, %SndNam3%
Gui, Add, Text, x12 y220 w312 h2 0x10
Gui, Add, Checkbox, % "x20 y222 w115 h16 vpin_Wave gpinWave Checked" pin_Wave (hasWave ? "" : " Disabled"), Pin Wave volume
Gui, Add, Checkbox, % "xp y+1 wp hp vrst_Wave grstWave Checked" rst_Wave (hasWave ? "" : " Disabled"), Restore Wave on exit
Gui, Add, Checkbox, % "xp y+1 wp hp vuseFadeMaster gsetFade Checked" useFadeMaster (useFadeMaster | useFadeWave ? "" : " Disabled"), Use fading for Master
Gui, Add, Checkbox, % "xp y+1 wp hp vuseFadeWave gsetFade Checked" useFadeWave (useFadeMaster | useFadeWave ? "" : " Disabled"), Use fading for Wave
Gui, Add, Text, x140 y222 w30 h13, Dim:
Gui, Add, Text, x140 y236 w30 h13 vlabelD, %vol_Dim% `%
Gui, Add, Text, x140 y255 w30 h13 vlblF, Fade:
Gui, Add, Text, x140 y269 w30 h13 vlabelF, % vol_FT / 1000 " s"
Gui, Add, Slider, x170 y222 w120 h30 vdimD gsetD AltSubmit Range1-100 TickInterval10 ToolTipBottom, %vol_Dim%
Gui, Add, Button, x291 y223 w25 h24 vresetD gresetD, Def
Gui, Add, Slider, x170 y255 w120 h30 vfadeT gsetF AltSubmit Range1000-8000 TickInterval500 Line250 Page500 ToolTipBottom, %vol_FT%
Gui, Add, Button, x291 y255 w25 h24 vresetF gresetF, Def
GuiControl, % (cards > 1 ? "Show" : "Hide"), s0
Loop, 3
	{
	GuiControl, % (A_Index > cards ? "Hide" : "Show"), s%A_Index%
	GuiControl, % ((cards < 2 OR A_Index > cards) ? "Hide" : "Show"), scard%A_Index%
	}
;================================================================
;					TAB 5 (General)
Gui, Tab, 5
i := 116/MonAct - 1
Gui, Font, s8
Gui, Add, GroupBox, x11 y+5 w312 h135, Detected monitor(s)
Gui, Font, s7
Gui, Add, Text, x20 yp+15 w10 h%i% vmn1 Hidden%debugR%, 1.
Gui, Add, Text, x+1 yp w285 hp vmd1 Hidden%debugR% %dbgP%Border, %MonNam1%
Gui, Add, Text, x20 y+1 w10 hp vmn2 Hidden%debugR%, 2.
Gui, Add, Text, x+1 yp w285 hp vmd2 Hidden%debugR% %dbgP%Border, %MonNam2%
Gui, Add, Text, x20 y+1 w10 hp vmn3 Hidden%debugR%, 3.
Gui, Add, Text, x+1 yp w285 hp vmd3 Hidden%debugR% %dbgP%Border, %MonNam3%
Gui, Add, Text, x20 y+1 w10 hp vmn4 Hidden%debugR%, 4.
Gui, Add, Text, x+1 yp w285 hp vmd4 Hidden%debugR% %dbgP%Border, %MonNam4%
Loop, 4
	{
	GuiControl, % (A_Index > MonAct ? "Hide" : "Show"), md%A_Index%
	GuiControl, % (A_Index > MonAct ? "Hide" : "Show"), mn%A_Index%
	}
i := 66/cards - 1
Gui, Font, s8
Gui, Add, GroupBox, x11 y170 w312 h85, Detected soundcard(s)
Gui, Font, s7
Gui, Add, Text, x20 yp+15 w10 h%i% Hidden%debugR% vsn1, 1.
Gui, Add, Text, x+1 yp w285 hp Hidden%debugR% %dbgP%Border vsd1, %SndNam1%
Gui, Add, Text, x20 y+1 w10 hp Hidden%debugR% vsn2, 2.
Gui, Add, Text, x+1 yp w285 hp Hidden%debugR% %dbgP%Border vsd2, %SndNam2%
Gui, Add, Text, x20 y+1 w10 hp Hidden%debugR% vsn3, 3.
Gui, Add, Text, x+1 yp w285 hp Hidden%debugR% %dbgP%Border vsd3, %SndNam3%
Loop, 3
	{
	GuiControl, % (A_Index > cards ? "Hide" : "Show"), sd%A_Index%
	GuiControl, % (A_Index > cards ? "Hide" : "Show"), sn%A_Index%
	}
Gui, Add, GroupBox, x11 y254 w312 h45,
Gui, Add, Checkbox, x20 y262 w100 h16 vnotray gnotray Section Checked%notray%, No tray icon
Gui, Add, Checkbox, xp y+2 w120 hp vauto gch Checked%auto%, Start with Windows
Gui, Add, Checkbox, x+5 ys w150 h16 vsavePos gch Checked%savePos%, Remember dialog position
Gui, Add, Text, xp y+2 hp 0x200, Show progress/splash for:
choice1 := vol_DT // 1000
Gui, Add, DropDownList, x+2 yp-3 w40 h18 r5 vshowtime gupdST Choose%choice1%, 1|2|3|4|5|6|7|8|9|10
Gui, Add, Text, x+2 yp+3 h16 0x200, sec.
;================================================================
;					TAB 6 (About)
Gui, Tab, 6
Gui, Font, s8
Gui, Add, GroupBox, x11 y53 w312 h168,
Gui, Font, s7
if ! A_IsCompiled
	Gui, Add, Picture, x140 y40 w32 h-1 ghome, %iconlocal%
else
	Gui, Add, Picture, x140 y40 w32 h-1 Icon1 ghome, %A_ScriptFullPath%
Gui, Add, Text, x16 y+10 h14 -Wrap, � Original script by Rajat, presented in
Gui, Add, Text, x+3 yp hp 0x100 -Wrap cBlue glink1, AHK Help showcase
Gui, Add, Text, x16 y+1 hp -Wrap, � Dimmer solution from
Gui, Add, Text, x+3 yp hp 0x100 -Wrap cBlue glink2, oxymor0n's script
Gui, Add, Text, x16 y+1 hp -Wrap, � Color Chooser function from
Gui, Add, Text, x+3 yp hp 0x100 -Wrap cBlue glink3, Majkinetor's 'Dlg' (ex 'CmnDlg')
Gui, Add, Text, x21 y+1 hp -Wrap, ^ licenced under
Gui, Add, Text, x+3 yp hp 0x100 -Wrap cBlue glink4, Creative Commons Attribution-Noncommercial
Gui, Add, Text, x16 y+1 hp -Wrap, � Color Picker trigger
Gui, Add, Text, x+3 yp hp 0x100 -Wrap cBlue glink5, solution by SKAN
Gui, Add, Text, x16 y+1 hp -Wrap, � About box code from
Gui, Add, Text, x+3 yp hp 0x100 -Wrap cBlue glink6, Accents by Skrommel
Gui, Add, Text, x16 y+1 hp -Wrap, � Vista+ compatibility code by Lexikos
Gui, Add, Text, x16 y+2 hp -Wrap, � Graphical interface and additional options by
Gui, Add, Text, x+3 yp hp 0x100 -Wrap cBlue gmyhome, Drugwash
Gui, Add, Text, x16 y+2 hp -Wrap, � Current version: %version%`, released %release%
Gui, Add, Text, x11 y+10 hp w312 h64 +0x1000 Center +Wrap,
	(LTrim Join
	VolOSD adds hotkey control with OSD capabilities to Master and Wave*
	`nsound output. Commands consist of: Master/Wave volume up/down,
	`nMaster 'dim to preset' volume, Master/Wave 'faded mute'.
	`n`n* Wave control only works in Win95/98/ME/NT/2000/XP/2003
	)
Gui, Font, s8
Gui, Tab
;================================================================
;					Tray window
Gui, 3:+AlwaysOnTop -Caption +Toolwindow +Border +LastFound
Gui, 3:Margin, 2, 2
GUi, 3:Add, Progress, y+5 w100 h14 -Smooth c%vol_CBM% Background%vol_CW% hwndTrayM vvol_Master Range0-100, %vol_Master%
GUi, 3:Add, Progress, y+6 w100 h14 -Smooth c%vol_CBW% Background%vol_CW% hwndTrayW vvol_Wave Range0-100, %vol_Wave%
if !themed
	{
	GuiControl, 3:+Smooth, vol_Master
	GuiControl, 3:+Smooth, vol_Wave
	}
DllCall("SendMessage" AW, Ptr, TrayM, "UInt", 0x410, "UInt", 2, "UInt", 0)	; PBM_SETSTATE PBST_ERROR
DllCall("SendMessage" AW, Ptr, TrayW, "UInt", 0x410, "UInt", 1, "UInt", 0)	; PBM_SETSTATE PBST_NORMAL
Gui, 3:Add, Button, x+2 ym w25 h20 +0x341 hwndhmutMaster gvol_MasterMute, M
Gui, 3:Add, Button, xp y+0 w25 h20 +0x341 hwndhmutWave gvol_WaveMute, W
DllCall("SendMessage" AW, Ptr, hmutMaster, "UInt", 0xF7, "UInt", 1, Ptr, hIcon1)	; BM_SETIMAGE
DllCall("SendMessage" AW, Ptr, hmutWave, "UInt", 0xF7, "UInt", 1, Ptr, hIcon1)	; BM_SETIMAGE
if !w9x
	{
	DllCall("uxtheme\SetWindowTheme", Ptr, hmutMaster, Ptr, &exploreW, "UInt", 0)
	DllCall("uxtheme\SetWindowTheme", Ptr, hmutWave, Ptr, &exploreW, "UInt", 0)
	}
if !hasWave%aud_DS%
	{
	GuiControl, 3:Hide, vol_Wave
	GuiControl, 3:Hide, W
	}
TrayVolWnd := WinExist()
Gui, 3:Show, AutoSize Hide, VolOSD TrayVol
hCursI := DllCall("LoadCursor", Ptr, NULL, "Int", 32513, Ptr)	;IDC_IBEAM
hCurs := DllCall("LoadCursor", Ptr, NULL, "Int", 32649, Ptr)		;IDC_HAND
OnMessage(0x200, "WM_MOUSEMOVE")
Subclass(TrayM, i := RegisterCallback("progWP", "", 4, hCursI))
Subclass(TrayW, i)
Subclass(TrayVolWnd, RegisterCallback("trayWP", "", 4, hCurs))
return
;================================================================
about:				; ABOUT box
;================================================================
Gui, 2:+Owner1 -MinimizeBox
Gui +Disabled
if ! A_IsCompiled
	Gui, 2:Add, Picture, x69 y10 w32 h-1 ghome, %iconlocal%
else
	Gui, 2:Add, Picture, x69 y10 w32 h-1 Icon1 ghome, %A_ScriptFullPath%
Gui, 2:Font, Bold
Gui, 2:Add, Text, x5 y+10 w160 Center, %appname% v%version%
Gui, 2:Font
Gui, 2:Add, Text, xp y+2 wp Center, by Drugwash
Gui, 2:Add, Text, xp y+2 wp Center gmail0 cBlue, %email%
Gui, 2:Add, Text, xp y+10 wp Center, Released %release%
Gui, 2:Add, Text, xp y+2 wp Center, (%type% version)
Gui, 2:Add, Text, xp y+10 wp Center, This product is open-source,
Gui, 2:Add, Text, xp y+2 wp Center, developed in AutoHotkey 1.0.48.05
Gui, 2:Font,CBlue Underline
Gui, 2:Add, Text, xp y+2 wp Center glink0, %link0%
Gui, 2:Font
Gui, 2:Add, Button, xp+40 y+20 w80 gdismiss Default, &OK
Gui, 2:Show,, About %appname%
return
;================================================================
dismiss:
Gui, 1:-Disabled
Gui, 2:Destroy
return
;================================================================
mail0:
mail0 := "mailto:" email
myhome:
home:
link0:
link1:
link2:
link3:
link4:
link5:
link6:
link := %A_ThisLabel%
Run, %link%,, UseErrorLevel
return
;#####################################################
;################# Find active monitor function ##################
;#####################################################
;================================================================
compute(a, b, c, d)
;================================================================
{
global vol_PosX, vol_PosY, vol_Width, vol_Thick, mutH, mutW
	vol_PosX := a + (c - a) // 2 - (vol_Width // 2)
	vol_PosY := b + (d - b) // 2 - vol_Thick
	mutH := a + (c - a) // 2 - (mutW // 2)
return
}

compare(a, b, c, d, e, f, g, h)	; ActX, ActY, ActW, ActH,  MonXLeft, MonXTop, MonXRight, MonXBottom
{
global vol_PosX, vol_PosY, vol_Width, vol_Thick, mutH, mutW
i := a + c // 2	; get center H
j :=  b + d // 2	; get center V
if ((i - e > 0) && (g - i > 0) && (j - f > 0) && (h - j > 0))
	{
	vol_PosX := e + (g - e) // 2 - (vol_Width // 2)
	vol_PosY := f + (h - f) // 2 - vol_Thick
	mutH := e + (g - e) // 2 - (mutW // 2)
	return true
	}
else
	return false
}
;================================================================
WM_MOUSEMOVE(wParam, lParam)
;================================================================
{
Global
Local wind, ctrl
WinGetTitle, wind
MouseGetPos,,,, ctrl
if wind in About VolOSD
	if ctrl in Static1,Static4,Static9
		return DllCall("SetCursor", Ptr, hCurs)
if wind in VolOSD options
	if ctrl in Static40,Static42,Static44,Static46,Static48,Static50,Static52,Static55
			,SysListView321,SysListView322,SysListView323
		return DllCall("SetCursor", Ptr, hCurs)
if wind in VolOSD TrayVol
	{
	if ctrl contains Button
		return DllCall("SetCursor", Ptr, hCurs)
;	if ctrl contains msctls_progress32
;		DllCall("SetCursor", Ptr, hCursI)
	}
}
;================================================================
Subclass(hCtrl, addr)
;================================================================
{
Global
DllCall("SetWindowLong", Ptr, hCtrl, "Int", -21	; GWL_USERDATA
	, Ptr, DllCall("GetWindowLong", Ptr, hCtrl, "Int", -4, Ptr))	; GWL_WNDPROC
DllCall("SetWindowLong", Ptr, hCtrl, "Int", -4, Ptr, addr)		; GWL_WNDPROC
}
;================================================================
trayWP(hwnd, msg, wP, lP)
;================================================================
{
Global
if (msg=0x6 && !wP)	; WM_ENABLE
	{
	WinHide, ahk_id %hwnd%
	return 0
	}
return DllCall("CallWindowProc", Ptr, DllCall("GetWindowLong", Ptr, hwnd, "Int", -21, Ptr)
			, Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)	; GWL_USERDATA
}
;================================================================
retray(wP, lP, msg, hwnd)
;================================================================
{
Global
if !notray
	{
	TrayIconSet(-1, TrayVolWnd)
	gosub setTray
	_TrayIconAct("options", 0, 0, 0)
	}
return DllCall("DefWindowProc", Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)
}
;================================================================
progWP(hwnd, msg, wP, lP)
;================================================================
{
Static seek, TME, uber, v, s
Global Ptr, AW, PtrSz
Critical
ofi := A_FormatInteger
SetFormat, Integer, H
hwnd+=0
msg+=0
SetFormat, Integer, %ofi%
if (uber="")
	uber := FileExist(A_WinDir "\System\uberskin.dll") ? "1" : "0"
if msg=0x2A3
	Tooltip
if msg not in 0x200,0x201,0x20A
	return DllCall("CallWindowProc", Ptr, DllCall("GetWindowLong", Ptr, hwnd, "Int", -21, Ptr)
				, Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)	; GWL_USERDATA
VarSetCapacity(TME, 12+PtrSz, 0)				; TRACKMOUSEEVENT struct
NumPut(16, TME, 0, "UInt")				; cbSize
NumPut(0x2, TME, 4, "UInt")				; dwFlags
NumPut(hwnd, TME, 8, Ptr)				; hwndTrack
NumPut(0xFFFFFFFF, TME, 8+PtrSz, "UInt")	; dwHoverTime
DllCall("_TrackMouseEvent", Ptr, &TME)		; comctl32.dll
mx := lP & 0xFFFF						; Get mouse horizontal position inside the control
my := lP>>16 & 0xFFFF					; Mouse vertical position will be used for vertical progress
ControlFocus,, ahk_id %hwnd%			; Use Focus as a trick...
GuiControlGet, v, 3:FocusV				; to get control's associated variable
ControlGetPos,,, cw,,, ahk_id %hwnd%	; Get control width
cw -= 2*uber							; Compensate for control border (1px left + 1px right) in Uberskin
VarSetCapacity(PBRANGE, 8, 0)
DllCall("SendMessage" AW, Ptr, hwnd, "UInt", 0x407, "UInt", 1, Ptr, &PBRANGE)	; PBM_GETRANGE
s := rL+((Abs(NumGet(PBRANGE, 4, "Int")  - (rL := NumGet(PBRANGE, 0, "Int"))))*++mx/cw)
if (msg=0x200 && seek != s)
	SetTimer, tt, -1
seek := s
if (msg=0x201 OR (msg=0x200 && (wP & 0x1)))
	{
	p := DllCall("SendMessage" AW, Ptr, hwnd, "UInt", 0x402, "UInt", s, "UInt", 0)	; PBM_SETPOS
	_progVar(v, s)
	return 1
	}
return DllCall("CallWindowProc", Ptr, DllCall("GetWindowLong", Ptr, hwnd, "Int", -21, Ptr)
			, Ptr, hwnd, "UInt", msg, Ptr, wP, Ptr, lP)	; GWL_USERDATA

tt:
Tooltip, % SubStr(v, 5) " " Round(s) " `%"
return
}
;================================================================
_progVar(var, val)
;================================================================
{
Global
Critical
if !var
	return
%var% := val
StringTrimLeft, obj, var, 4
SoundSet, %val%, %obj%, Volume, %aud_DS%
SetTimer, getval, -1
}
;================================================================
DS_EnumProc(guid, desc, name, ctrl)
;================================================================
{
Global
if !guid
	return 1
cards++
SoundGet, hasWave%cards%, WAVE, VOLUME, %cards%
hasWave := hasWave | hasWave%cards%
VarSetCapacity(SndNam%cards%, 255, 0)
DllCall("msvcrt\memcpy", Ptr, &SndNam%cards%, Ptr, desc, "UInt", 255, "CDecl")
VarSetCapacity(SndNam%cards%, -1)
VarSetCapacity(guid%cards%, 16, 0)
DllCall("msvcrt\memcpy", Ptr, &guid%cards%, Ptr, guid, "UInt", 16, "CDecl")
VarSetCapacity(guid%cards%, -1)
;MakeGUID(guid, sguid)
return 1
}
;================================================================
#include lib
#include func_MonitorDetection 2.0.ahk
#include func_TrayIcon 1.2d.ahk
#include func_ChgColor 1.9.4.ahk
#include func_Conversions.ahk

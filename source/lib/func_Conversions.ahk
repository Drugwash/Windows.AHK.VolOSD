; collection of conversion functions
; �Drugwash September 2011

_MB2WC(in, ByRef out, cp="65001")
{
Static conv
if !conv
	conv := A_OSType="WIN32_WINDOWS" ? "unicows\" : ""
if !sz := DllCall(conv "MultiByteToWideChar", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", 0)	; CP_UTF8
	sz := DllCall("MultiByteToWideChar", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", 0)	; CP_UTF8
;	sz := DllCall("MultiByteToWideChar", "UInt", 0, "UInt", 0x1, "UInt", &in, "Int", -1, "UInt", &out, "UInt", 0)	; CP_ACP MB_PRECOMPOSED
sz*=2
VarSetCapacity(out, sz+1, 0)
if !DllCall(conv "MultiByteToWideChar", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", sz)	; CP_UTF8
	if !DllCall("MultiByteToWideChar", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", sz)	; CP_UTF8
;	if !DllCall("MultiByteToWideChar", "UInt", 0, "UInt", 0x1, "UInt", &in, "Int", -1, "UInt", &out, "UInt", sz)	; CP_ACP MB_PRECOMPOSED
		msgbox, % "error " DllCall("GetLastError") " in " A_ThisFunc "() - converting to Unicode, sz=" sz ", text:`n" in
VarSetCapacity(out, -1)
return &out
}
; **************************************
_WC2MB(in, cp="65001")
{
Static conv
if !conv
	conv := A_OSType="WIN32_WINDOWS" ? "unicows\" : ""
if !sz := DllCall(conv "WideCharToMultiByte", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", 0, "UInt", 0, "UInt", 0)	; CP_UTF8
	if !sz := DllCall("WideCharToMultiByte", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", 0, "UInt", 0, "UInt", 0)	; CP_UTF8
		msgbox, % "error " DllCall("GetLastError") " in " A_ThisFunc "() - converting to ANSI, sz=" sz "`ntext: " in
VarSetCapacity(out, sz+1, 0)
if !DllCall(conv "WideCharToMultiByte", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", sz, "UInt", 0, "UInt", 0)	; CP_UTF8
	DllCall("WideCharToMultiByte", "UInt", cp, "UInt", 0, "UInt", &in, "Int", -1, "UInt", &out, "UInt", sz, "UInt", 0, "UInt", 0)	; CP_UTF8
VarSetCapacity(out, -1)
return nt
}
; **************************************
